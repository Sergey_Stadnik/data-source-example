//
//  ViewController.m
//  DataSourceTest
//
//  Created by Sergey on 5/3/17.
//  Copyright © 2017 Sergey. All rights reserved.
//

#import "ViewController.h"
#import "InfoTableViewCell.h"
#import "SomeService.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITableView *infoTableView;
@property (nonatomic, strong) SomeService* service;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UINib *nib = [UINib nibWithNibName:@"InfoTableViewCell" bundle:nil];
    [[self infoTableView] registerNib:nib forCellReuseIdentifier:@"InfoTableViewCell"];
    [self configure];
}

- (void)configure {
    self.service = [[SomeService alloc] initWithTableView:self.infoTableView];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 25;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    InfoTableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"InfoTableViewCell"];
    cell.infoLabel.text = @"View controller";
    return cell;
}


@end
