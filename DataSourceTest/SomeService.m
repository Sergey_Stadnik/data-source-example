//
//  SomeService.m
//  DataSourceTest
//
//  Created by Sergey on 5/3/17.
//  Copyright © 2017 Sergey. All rights reserved.
//

#import "SomeService.h"
#import "InfoTableViewCell.h"

@interface SomeService () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) UITableView *tableView;
@property (weak, nonatomic) id<UITableViewDataSource> originalDataSource;
@property (weak, nonatomic) id<UITableViewDelegate> originalDelegate;

@end

@implementation SomeService

- (instancetype)initWithTableView:(UITableView*)tableView {
    self = [super init];
    if (self) {
        self.tableView = tableView;
        self.originalDataSource = tableView.dataSource;
        self.originalDelegate = tableView.delegate;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
    }
    return self;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.originalDataSource tableView:tableView numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row % 2 == 0) {
        InfoTableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"InfoTableViewCell"];
        cell.infoLabel.text = @"Service";
        return cell;
    } else {
        return [self.originalDataSource tableView:tableView cellForRowAtIndexPath:indexPath];
    }
}

@end
