//
//  AppDelegate.h
//  DataSourceTest
//
//  Created by Sergey on 5/3/17.
//  Copyright © 2017 Sergey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

