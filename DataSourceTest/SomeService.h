//
//  SomeService.h
//  DataSourceTest
//
//  Created by Sergey on 5/3/17.
//  Copyright © 2017 Sergey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SomeService : NSObject

- (instancetype)initWithTableView:(UITableView*)tableView;

@end
